package com.alox1d.pcbcalc.models;

public class DeviceFailureRateComponentItem {
    private String name;
    private Double componentFailureRateInNormal;
    private Double loadingCoeff;
    private Double correctionCoeff;
    private Double componentFailureRateConsideringExternalFactors;
    private Double componentFailureRateInWork;
    private Double componentFailureRateOfGroup;

}
