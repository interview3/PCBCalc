package com.alox1d.pcbcalc.utils;

import android.os.Environment;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTF;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTFunc;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTOMath;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTOMathArg;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTOMathPara;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTRad;
import org.openxmlformats.schemas.officeDocument.x2006.math.CTSSup;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTOnOff;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTbl;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.STOnOff;
import org.w3c.dom.Node;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ApachePOIHelper {
    private static final String FONT_NAME = "Times New Roman";
    private static final String BOOKMARK_REGEX = "_[1-8]$";
    private XWPFDocument doc = null;

//    private String templateFileName;
//    private final String templateFolderName = "Documents";

    private boolean replaced;
    private ArrayList<String> replacedBookmarks;
    private ArrayList<String> allBookmarks;
    //getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)
    //+ "/docs.docx";

    public void openDocument(InputStream inputStreamDoc) throws Exception {
        fixForAndroidApachePOI();

//        this.templateFileName = templateFileName;
        doc = new XWPFDocument(inputStreamDoc);
        inputStreamDoc.close();


//        run.setText("<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfrac><mn>1</mn><mn>2</mn></mfrac></math>");
//        String folderName = "Documents";
//        String filePath = Environment.getExternalStorageDirectory().getPath() + File.separator + folderName + File.separator;
//        String filePathWithName = filePath + templateFileName;
//        doc.write(new FileOutputStream(filePathWithName));

//        final OPCPackage opcPackage = OPCPackage.open(filePathWithName);
//        addCustomXmlPart(opcPackage);
//        opcPackage.save(new File(filePathWithName));

//        Intent intentSave = new Intent();
//        intentSave.setType("audio/*");
//        intentSave.setAction(Intent.CONTEN);
//        startActivityForResult(intentSave,1);
    }
//    private static void addCustomXmlPart(OPCPackage opcPackage) throws IOException, InvalidFormatException {
//        final PackagePartName partName = PackagingURIHelper.createPartName("/customXml/item1.xml");
//        final PackagePart part = opcPackage.createPart(partName, ContentTypes.PLAIN_OLD_XML);
//        final OutputStream outputStream = part.getOutputStream();
//        outputStream.write("<test>A</test>".getBytes());
//        outputStream.close();
//
//        part.addRelationship(partName, TargetMode.INTERNAL, PackageRelationshipTypes.CUSTOM_XML);
//
//        final PackagePartName workbookName = PackagingURIHelper.createPartName("/xl/workbook.xml");
//        final PackagePart workbookPart = opcPackage.getPart(workbookName);
//        workbookPart.addRelationship(partName, TargetMode.INTERNAL, PackageRelationshipTypes.CUSTOM_XML);
//    }


    private void fixForAndroidApachePOI() {
        System.setProperty("org.apache.poi.javax.xml.stream.XMLInputFactory", "com.fasterxml.aalto.stax.InputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLOutputFactory", "com.fasterxml.aalto.stax.OutputFactoryImpl");
        System.setProperty("org.apache.poi.javax.xml.stream.XMLEventFactory", "com.fasterxml.aalto.stax.EventFactoryImpl");
    }


//    private String getMathML(CTOMath ctomath) throws Exception {
//        TransformerFactory tFactory = TransformerFactory.newInstance();
//        StreamSource stylesource = new StreamSource(inputStreamStylesheet);
//
//        javax.xml.transform.Transformer transformer = tFactory.newTransformer(stylesource);
//
//        Node node = ctomath.getDomNode();
//
//        DOMSource source = new DOMSource(node);
//        StringWriter stringwriter = new StringWriter();
//        StreamResult result = new StreamResult(stringwriter);
//        transformer.setOutputProperty("omit-xml-declaration", "yes");
//        transformer.transform(source, result);
//
//        String mathML = stringwriter.toString();
//        stringwriter.close();
//
//        //The native OMML2MML.XSL transforms OMML into MathML as XML having special name spaces.
//        //We don't need this since we want using the MathML in HTML, not in XML.
//        //So ideally we should changing the OMML2MML.XSL to not do so.
//        //But to take this example as simple as possible, we are using replace to get rid of the XML specialities.
//        mathML = mathML.replaceAll("xmlns:m=\"http://schemas.openxmlformats.org/officeDocument/2006/math\"", "");
//        mathML = mathML.replaceAll("xmlns:mml", "xmlns");
//        mathML = mathML.replaceAll("mml:", "");
//
//        return mathML;
//    }


    public void findAndReplace(
            String bookmarkName, String bookmarkValue) {
        Iterator<XWPFParagraph> paragraphIter = null;
        XWPFParagraph paragraph = null;
        List<CTBookmark> bookmarkList = null;
        Iterator<CTBookmark> bookmarkIter = null;
        CTBookmark bookmark = null;
        XWPFRun run = null;
        Node nextNode = null;
        List<XWPFTable> tableList = null;
        Iterator<XWPFTable> tableIter = null;
        List<XWPFTableRow> rowList = null;
        Iterator<XWPFTableRow> rowIter = null;
        List<XWPFTableCell> cellList = null;
        Iterator<XWPFTableCell> cellIter = null;
        XWPFTable table = null;
        XWPFTableRow row = null;
        XWPFTableCell cell = null;

        List<CTOMath> mCTOMaths = null;
        List<CTOMathPara> mCTOMathParas = null;
        List<CTOMath> mCTOMathsOfCell = null;
        List<CTOMathPara> mCTOMathsParaOfCell = null;
        replacedBookmarks = new ArrayList<>();
        allBookmarks = new ArrayList<>();

        List<XWPFParagraph> paragraphs = collectParagraphs();
//        List<IBodyElement> l = doc.getBodyElements();
//        l.get(1).getBody().getParagraphs()
        // Get an Iterator to step through the contents of the paragraph list.
        paragraphIter = paragraphs.iterator();
        while (paragraphIter.hasNext()) {
            // Get the paragraph, a llist of CTBookmark objects and an Iterator
            // to step through the list of CTBookmarks.
            paragraph = paragraphIter.next();


            mCTOMaths = paragraph.getCTP().getOMathList();
            mCTOMathParas = paragraph.getCTP().getOMathParaList();
            CTP ctp = paragraph.getCTP();

            checkForReplace(bookmarkName, bookmarkValue, paragraph, ctp);
            for (CTOMath ctomath : mCTOMaths) {
                checkForReplace(bookmarkName, bookmarkValue, paragraph, ctomath);
                replaceInnersOfMath(bookmarkName, bookmarkValue, paragraph, ctomath);

            }
            for (CTOMathPara ctomathPara : mCTOMathParas) {
                List<CTOMath> CTOMathOfPara = ctomathPara.getOMathList();
                for (CTOMath ctoMath : CTOMathOfPara) {
                    checkForReplace(bookmarkName, bookmarkValue, paragraph, ctoMath);
                    replaceInnersOfMath(bookmarkName, bookmarkValue, paragraph, ctoMath);
                }

            }
//            for (CTOMathPara ctoMathPara : mCTOMathParas){
//                for (CTOMath ctomath : ctoMathPara.getOMathList()) {
////                boolean replaced = checkForReplace(bookmarkName, bookmarkValue, paragraph, ctomath);
//                    checkForReplace(bookmarkName, bookmarkValue, paragraph, ctomath);
////                if (replaced) break;
//                }
//            }


//            for (IBodyElement ibodyelement : doc.getBodyElements()) {
//                if (ibodyelement.getElementType().equals(BodyElementType.PARAGRAPH)) {
//                    XWPFParagraph par = (XWPFParagraph) ibodyelement;
//                    for (CTOMath ctomath : par.getCTP().getOMathList()) {
//                        checkForReplace(bookmarkName, bookmarkValue, par, ctomath);
//
//                    }
//                    for (CTOMathPara ctomathpara : par.getCTP().getOMathParaList()) {
//                        for (CTOMath ctomath : ctomathpara.getOMathList()) {
//                            checkForReplace(bookmarkName, bookmarkValue, par, ctomath);
//
//                        }
//                    }
//                } else if (ibodyelement.getElementType().equals(BodyElementType.TABLE)) {
//                    XWPFTable table1 = (XWPFTable) ibodyelement;
//                    for (XWPFTableRow row1 : table1.getRows()) {
//                        for (XWPFTableCell cell1 : row1.getTableCells()) {
//                            for (XWPFParagraph paragraph1 : cell1.getParagraphs()) {
//                                for (CTOMath ctomath : paragraph1.getCTP().getOMathList()) {
//                                    checkForReplace(bookmarkName, bookmarkValue, paragraph1, ctomath);
//
//                                }
//                                for (CTOMathPara ctomathpara : paragraph1.getCTP().getOMathParaList()) {
//                                    for (CTOMath ctomath : ctomathpara.getOMathList()) {
//                                        checkForReplace(bookmarkName, bookmarkValue, paragraph1, ctomath);
//
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }


//            tableList = doc.getTables();
//            tableIter = tableList.iterator();
//            while (tableIter.hasNext()) {
//                table = tableIter.next();
//                rowList = table.getRows();
//                rowIter = rowList.iterator();
//                while (rowIter.hasNext()) {
//                    row = rowIter.next();
//                    cellList = row.getTableCells();
//                    cellIter = cellList.iterator();
//                    while (cellIter.hasNext()) {
//                        cell = cellIter.next();
//                        for (XWPFParagraph paragraphCell : cell.getParagraphs()) {
////                            this.checkForReplace(bookmarkName, bookmarkValue, paragraphCell, table.getCTTbl());
//                            mCTOMathsOfCell = paragraphCell.getCTP().getOMathList();
//                            mCTOMathsParaOfCell = paragraphCell.getCTP().getOMathParaList();
//                            for (CTOMath ctomath : mCTOMathsOfCell) {
//                                checkForReplace(bookmarkName, bookmarkValue, paragraphCell, ctomath);
//
//                            }
//                            for (CTOMathPara ctomathPara : mCTOMathsParaOfCell) {
//                                List<CTOMath> CTOMathOfPara = ctomathPara.getOMathList();
//                                for (CTOMath ctoMath : CTOMathOfPara) {
//                                    checkForReplace(bookmarkName, bookmarkValue, paragraphCell, ctoMath);
//
//                                    replaceInnersOfMath(bookmarkName, bookmarkValue, paragraphCell,ctoMath);
//                                }
//
//                            }
//                        }
//                    }
//                }
//            }
        }
    }

    private void replaceInnersOfMath(String bookmarkName, String bookmarkValue, XWPFParagraph paragraph, XmlObject xmlObject) {
        CTOMath ctoMath;
        CTOMathArg ctoMathArg;
        List<CTRad> mainRadList = null;

        List<CTF> ctfs = null;
        List<CTSSup> ssups = null;
        if (xmlObject instanceof CTOMath) {
            ctoMath = (CTOMath) xmlObject;

            ctfs = ctoMath.getFList();
            ssups = ctoMath.getSSupList();
            List<CTFunc> ctfuncs = ctoMath.getFuncList();
            mainRadList = ctoMath.getRadList();
            ctoMath.getSSubList();
        } else if (xmlObject instanceof CTOMathArg) {
            ctoMathArg = (CTOMathArg) xmlObject;
            ctfs = ctoMathArg.getFList();
            ssups = ctoMathArg.getSSupList();

            mainRadList = ctoMathArg.getRadList();


        } else {
            int i = 0;
        }

        for (CTF ctf : ctfs) {
            CTOMathArg num = ctf.getNum();
            replaceInnersOfMath(bookmarkName, bookmarkValue, paragraph, num);
            checkForReplace(bookmarkName, bookmarkValue, paragraph, num);

            List<CTSSup> ctsSupsNum = num.getSSupList();
            for (CTSSup ctsSup : ctsSupsNum) {
                replaceInnersOfMath(bookmarkName, bookmarkValue, paragraph, ctsSup.getE());
                replaceInnersOfMath(bookmarkName, bookmarkValue, paragraph, ctsSup.getSup());
                checkForReplace(bookmarkName, bookmarkValue, paragraph, ctsSup.getE());

            }


            CTOMathArg den = ctf.getDen();
            replaceInnersOfMath(bookmarkName, bookmarkValue, paragraph, den);
            checkForReplace(bookmarkName, bookmarkValue, paragraph, den);

            List<CTSSup> ctsSupsDen = den.getSSupList();
            for (CTSSup ctsSup : ctsSupsDen) {
                replaceInnersOfMath(bookmarkName, bookmarkValue, paragraph, ctsSup.getE());
                checkForReplace(bookmarkName, bookmarkValue, paragraph, ctsSup.getE());
            }
            List<CTRad> ctRads = den.getRadList();
            for (CTRad ctRad : ctRads) {
                replaceInnersOfMath(bookmarkName, bookmarkValue, paragraph, ctRad.getE());


                checkForReplace(bookmarkName, bookmarkValue, paragraph, ctRad.getE());

            }


        }
        for (CTRad ctRad : mainRadList){
            replaceInnersOfMath(bookmarkName, bookmarkValue, paragraph, ctRad.getE());
            checkForReplace(bookmarkName, bookmarkValue, paragraph, ctRad.getE());
        }
        for (CTSSup sSup : ssups){
            replaceInnersOfMath(bookmarkName, bookmarkValue, paragraph, sSup.getSup());
            checkForReplace(bookmarkName, bookmarkValue, paragraph, sSup.getSup());

            replaceInnersOfMath(bookmarkName, bookmarkValue, paragraph, sSup.getE());
            checkForReplace(bookmarkName, bookmarkValue, paragraph, sSup.getE());
        }




    }
//    private void recursive(XWPFParagraph paragraph, CTOMath ctoMath) {

    //    private static void addField(XWPFParagraph paragraph, String fieldName) {
//        List<CTSimpleField> ctSimpleField = paragraph.getCTP().getFldSimpleList();
//        for (CTSimpleField ctSimpleField : ctSimpleField){
//            ctSimpleField.setInstr(fieldName + " \\* MERGEFORMAT ");
//            ctSimpleField.addNewR().addNewT().setStringValue("<<fieldName>>");
//        }
//
//    }
    private List<XWPFParagraph> collectParagraphs() {
        List<XWPFParagraph> paragraphs = new ArrayList<>();
        paragraphs.addAll(doc.getParagraphs());

        for (XWPFTable table : doc.getTables()) {
            for (XWPFTableRow row : table.getRows()) {
                for (XWPFTableCell cell : row.getTableCells())

                    paragraphs.addAll(cell.getParagraphs());
            }
        }
        return paragraphs;
    }


    private void checkForReplace(String bookmarkName, String bookmarkValue, XWPFParagraph paragraph, XmlObject xmlObject) {
        List<CTBookmark> bookmarkList = null;
        Iterator<CTBookmark> bookmarkIter;
        CTBookmark bookmark;
        XWPFRun run;
        Node nextNode;

        if (xmlObject instanceof CTP) {
            bookmarkList = ((CTP) xmlObject).getBookmarkStartList();
        } else if (xmlObject instanceof CTOMath) {
            bookmarkList = ((CTOMath) xmlObject).getBookmarkStartList();
        } else if (xmlObject instanceof CTOMathArg) {
            bookmarkList = ((CTOMathArg) xmlObject).getBookmarkStartList();
        } else if (xmlObject instanceof CTTbl) {
            bookmarkList = ((CTTbl) xmlObject).getBookmarkStartList();
        }
//                    for (CTOMathPara ctomathpara : paragraph.getCTP().getOMathParaList()) {
//                        for (CTOMath ctomath : ctomathpara.getOMathList()) {
//                            mathMLList.add(getMathML(ctomath));
//                        }
//                    }

        bookmarkIter = bookmarkList.iterator();

        while (bookmarkIter.hasNext()) {
            // Get a Bookmark and check it's name. If the name of the
            // bookmark matches the name the user has specified...
            bookmark = bookmarkIter.next();

            String bookmarkGetName = bookmark.getName();
            allBookmarks.add(bookmarkGetName);

            if (bookmarkGetName.equals(bookmarkName) && !replacedBookmarks.contains(bookmarkName)) {
                replace(bookmarkValue, paragraph, xmlObject, bookmark);
                replacedBookmarks.add(bookmarkGetName);
//                return true;

            } else {
                if (bookmarkGetName.matches(bookmarkName + BOOKMARK_REGEX)) {
                    replace(bookmarkValue, paragraph, xmlObject, bookmark);
                    replacedBookmarks.add(bookmarkGetName);

//                    return true;

                }
            }

        }
//        return false;

    }


    private void replace(String bookmarkValue, XWPFParagraph paragraph, XmlObject xmlObject, CTBookmark bookmark) {
        XWPFRun run;
        Node nextNode;// ...create the text run to insert and set it's text
        // content and then insert that text into the document.
        run = paragraph.createRun();
//        CTR ctr = run.getCTR();
//        CTRPr cTRPr = run.getCTR().getRPr();
//        cTRPr.addNewOMath().setVal(STOnOff.Enum);
//        ctr.setRPr(cTRPr);

        CTOnOff ctonoff = CTOnOff.Factory.newInstance();
//        ctonoff.setVal(STOnOff.TRUE);
        CTRPr ctrPr = null;
        try {
            ctrPr = CTRPr.Factory.parse(run.getCTR().getDomNode());
        } catch (XmlException e) {
            e.printStackTrace();
        }
        CTOnOff ctonoffmath = ctrPr.addNewOMath();
        ctonoffmath.setVal(STOnOff.ON);
        ctrPr.setOMath(ctonoffmath);

//        POIXMLProperties properties = doc.getProperties();
//        POIXMLProperties.CustomProperties customProperties = properties.getCoreProperties();
//        XWPFRun runn = new XWPFRun()
        run.setText(bookmarkValue);
        run.setFontFamily(FONT_NAME);
        run.setFontSize(14);
//                        if(defaultStyle != null || style != null){
//                            //ctrPr.addNewRFonts().setAscii("Calibri");
//                            if(style != null){
//                                applyStyleToRun(style, run);
//                            }else{
//                                applyStyleToRun(defaultStyle, run);
//                            }
//
//                        }

        // The new Run should be inserted between the bookmarkStart
        // and bookmarkEnd nodes, so find the bookmarkEnd node.
        // Note that we are looking for the next sibling of the
        // bookmarkStart node as it does not contain any child nodes
        // as far as I am aware.
        nextNode = bookmark.getDomNode().getNextSibling();
        // If the next node is not the bookmarkEnd node, then step
        // along the sibling nodes, until the bookmarkEnd node
        // is found. As the code is here, it will remove anything
        // it finds between the start and end nodes. This, of course
        // comepltely sidesteps the issues surrounding boorkamrks
        // that contain other bookmarks which I understand can happen.
        while (nextNode != null && nextNode.getNodeName() != null && !(nextNode.getNodeName().contains("bookmarkEnd"))) {
            xmlObject.getDomNode().removeChild(nextNode);
            nextNode = bookmark.getDomNode().getNextSibling();
        }

        // Finally, insert the new Run node into the document
        // between the bookmarkStart and the bookmarkEnd nodes.
        xmlObject.getDomNode().insertBefore(run.getCTR().getDomNode(), nextNode);
    }
//    private void replace(String bookmarkValue, XWPFParagraph paragraph, XmlObject xmlObject, CTBookmark bookmark) {
//        XWPFRun run;
//        Node nextNode;// ...create the text run to insert and set it's text
//        // content and then insert that text into the document.
//        run = paragraph.createRun();
////        CTR ctr = run.getCTR();
////        CTRPr cTRPr = run.getCTR().getRPr();
////        cTRPr.addNewOMath().setVal(STOnOff.Enum);
////        ctr.setRPr(cTRPr);
//
////        CTOnOff ctonoff = CTOnOff.Factory.newInstance();
////        ctonoff.setVal(STOnOff.TRUE);
////        run.getCTR().getRPr().setOMath(ctonoff);
//
////        POIXMLProperties properties = doc.getProperties();
////        POIXMLProperties.CustomProperties customProperties = properties.getCoreProperties();
////        XWPFRun runn = new XWPFRun()
//        run.setText(bookmarkValue);
//        run.setFontFamily(FONT_NAME);
//        run.setFontSize(14);
////                        if(defaultStyle != null || style != null){
////                            //ctrPr.addNewRFonts().setAscii("Calibri");
////                            if(style != null){
////                                applyStyleToRun(style, run);
////                            }else{
////                                applyStyleToRun(defaultStyle, run);
////                            }
////
////                        }
//
//        // The new Run should be inserted between the bookmarkStart
//        // and bookmarkEnd nodes, so find the bookmarkEnd node.
//        // Note that we are looking for the next sibling of the
//        // bookmarkStart node as it does not contain any child nodes
//        // as far as I am aware.
//        nextNode = bookmark.getDomNode().getNextSibling();
//        // If the next node is not the bookmarkEnd node, then step
//        // along the sibling nodes, until the bookmarkEnd node
//        // is found. As the code is here, it will remove anything
//        // it finds between the start and end nodes. This, of course
//        // comepltely sidesteps the issues surrounding boorkamrks
//        // that contain other bookmarks which I understand can happen.
//        while (nextNode != null && nextNode.getNodeName() != null && !(nextNode.getNodeName().contains("bookmarkEnd"))) {
//            xmlObject.getDomNode().removeChild(nextNode);
//            nextNode = bookmark.getDomNode().getNextSibling();
//        }
//
//        // Finally, insert the new Run node into the document
//        // between the bookmarkStart and the bookmarkEnd nodes.
//        xmlObject.getDomNode().insertBefore(run.getCTR().getDomNode(), nextNode);
//    }
    public String saveCurrentDocument(String folderName, String fileName) throws IOException {
        File root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        String folderPath = root.getAbsolutePath() + File.separator;
        String filePath = folderPath + fileName;

        File dir = new File(folderPath);
        if (!dir.exists()){
            dir.mkdir();
        }

        FileOutputStream fileOutputStream = new FileOutputStream(filePath);
        doc.write(fileOutputStream);

//        Intent intentSave = new Intent();
//        intentSave.setType("audio/*");
//        intentSave.setAction(Intent.ACTION_MEDIA_MOUNTED);
//        startActivityForResult(intentSave,1);

//        new Intent(Intent.ACTION_MEDIA_MOUNTED,
//                Uri.parse("file://" + Environment.getExternalStorageDirectory()));

        doc.close();

        return folderPath;
    }

}
//Workbook wb = WorkbookFactory.create(getAssets().open("test.xls"));

//        WordprocessingMLPackage wordPackage = null;
//        File tempFile = null;
//        String file = null;
//        File doc = null;
//        try {
//            inputStream = getResources().openRawResource(R.raw.mechanical_impacts);
//            input = getAssets().open("mechanical_impacts.docx");
//            //getAssets().open()
//            String FilePath = Environment.getExternalStorageDirectory().getPath() + File.separator;
//            file = FilePath + "My1.docx";
//            doc = new File(file);
//            tempFile = File.createTempFile("mechanical_impacts", ".docx");
//            tempFile.deleteOnExit();
//            FileOutputStream out = new FileOutputStream(tempFile);
//            FileInputStream fileInputStream = new FileInputStream(doc);
//            IOUtils.copy(inputStream, out);
////            final LoadFromZipNG loader = new LoadFromZipNG();
////            wordPackage= (WordprocessingMLPackage)loader.get(inputStream);
//            //raw = this.getAssets().open("mechanical_impacts.docx");
////            JAXBContext.newInstance("org.docx4j.openpackaging.contenttype");
////            URI path = new URI("android.resource://com.alox1d.pcbcalc/" + R.raw.mechanical_impacts);
//            wordPackage = WordprocessingMLPackage.load(fileInputStream);
//            //mlp = WordprocessingMLPackage.load(new File("android.resource://com.alox1d.pcbcalc/raw/"+ R.raw.mechanical_impacts));
//        } catch (Docx4JException e) {
//            e.printStackTrace();
//        }
//         catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            Docx4jHelper.replaceText(wordPackage.getMainDocumentPart());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


//        Thread timer = new Thread() {
//            public void run() {
//
//                WordprocessingMLPackage wordPackage = null;
//                try {
//                    wordPackage = WordprocessingMLPackage.createPackage();
//                } catch (InvalidFormatException e) {
//                    e.printStackTrace();
//                }
//                MainDocumentPart mainDocumentPart = wordPackage.getMainDocumentPart();
//                mainDocumentPart.addStyledParagraphOfText("Title", "Hello World!");
//                mainDocumentPart.addParagraphOfText("Welcome To Baeldung");
//                String FilePath = Environment.getExternalStorageDirectory().getPath() + File.separator;
//                String File = FilePath + "My22.docx";
//                File exportFile = new File(File);
//                try {
//                    wordPackage.save(exportFile);
//                } catch (Docx4JException e) {
//                    e.printStackTrace();
//                }
//            }
//        };
//        timer.start();


//        //Blank Document
//        XWPFDocument document = new XWPFDocument();
//
//        //Write the Document in file system
//        String FilePath = Environment.getExternalStorageDirectory().getPath() + File.separator;
//        String File = FilePath + "MyApache.docx";
//        FileOutputStream out = null;
//        try {
//            out = new FileOutputStream( new File(File));
//            document.write(out);
//            out.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


//        Thread timer  = new Thread(){
//            public void run(){
//                try
//                {
//                    String FilePath = Environment.getExternalStorageDirectory().getPath() + File.separator;
//                    String File = FilePath + "My.docx";
//
//                    Document doc = new Document(File);
//
//                    DocumentBuilder builder = new DocumentBuilder(doc);
//
//// Specify font formatting before adding text.
//
//                    builder.write("Insert text");
//                    doc.save(File);
//                }
//                catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
//            }
//        };
//        timer.start();